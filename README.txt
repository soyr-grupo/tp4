PING-PONG Buffer usando Hilos en C

Este proyecto implementa un buffer PING-PONG en C utilizando hilos, variables compartidas y mutex. El programa consta de dos productores (hilos) que leen datos desde un archivo y los escriben en un buffer ping-pong, 
y un consumidor (hilo) que lee desde el buffer ping-pong, imprime los valores en pantalla y los almacena en otro archivo.
Instrucciones de Uso

    Compilación:

    Compilar el programa utilizando un compilador de C (por ejemplo, GCC):

    bash

gcc -o pinpon pinpon.c -lpthread

Ejecución:

Ejecutar el programa:

bash

./pinpon

Instrucciones adicionales:

    Para cambiar de productor: Envía la señal SIGUSR1.

    Para finalizar el programa: Envía la señal SIGTERM.

    Nota: Puedes encontrar el PID del programa ejecutando ps aux | grep pinpon en otra ventana.

Detalles de Implementación

    Variables Compartidas:

        buffer: Matriz para almacenar datos en un formato ping-pong.

        conteo: Contador para rastrear la posición actual en el buffer.

        fin_de_lectura: Bandera para indicar el final de la lectura del archivo.

        cambio: Variable para controlar el cambio de productor.

        current_producer: Variable global para indicar el productor actual.

        salir_del_bucle: Variable de condición adicional.

    Funciones:

        productor: Función que implementa la lógica del productor.

        consumidor: Función que implementa la lógica del consumidor.

        sigusr_handler: Manejador de señales para cambiar de productor (SIGUSR1) o finalizar el programa (SIGTERM).

Archivos de Entrada y Salida

    Archivo de Entrada (pg2000.txt):

    El archivo de entrada contiene líneas de texto que serán producidas por los productores.

    Archivo de Salida (CONSUMIDOR.txt):

    El archivo de salida contendrá las líneas de texto consumidas por el hilo consumidor.

Compilación y Ejecución

Tener un compilador de C y la biblioteca de hilos instalada. 
Compila el programa utilizando el comando mencionado anteriormente y ejecútalo. 
Sigue las instrucciones adicionales para cambiar de productor o finalizar el programa.
