#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>

#define NUM_THREAD 2
#define MAX_BUFFER 5
#define KByte 1024

char buffer[MAX_BUFFER][KByte];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
int conteo = 0,a = 0;
int fin_de_lectura = 0; // Bandera para indicar el final de la lectura del archivo
char cambio = '1';
int current_producer = 0; // Variable global para indicar el productor actual
int salir_del_bucle = 0; // Variable de condición adicional

void* productor(void* arg);
void* consumidor(void* arg);
void sigusr_handler(int signo);

int main() {
    pthread_t hilos_productor[NUM_THREAD];
    pthread_t hilo_consumidor;

    char productor_ids[2] = {'1', '2'};
 
    // Configurar el manejador de señales
    signal(SIGUSR1, sigusr_handler);
    signal(SIGTERM, sigusr_handler);
    printf("\n\t>>    INSTRUCCIONES \n");
    printf("\t>>ingrese en otra venta:   ps aux | grep <nombre_ejecutable>\n");
    printf("\t>>una vez obtenido el PID ingrese: \n");
    printf("\t>>SIGUSR1 para cambiar de productor \n");
    printf("\t>>SIGTERM para finalizar el ejecutable \n\n");
    //sleep(10); //espera para que el usuario lea las instrucciones

    printf("\t************************************************\n");
    printf("\tcreando productor %c ....\n", productor_ids[0]);
    pthread_create(&hilos_productor[0], NULL, productor, (void *)&productor_ids[0]);
    sleep(1);
    printf("\t************************************************\n");
    printf("\tcreando productor %c ...\n", productor_ids[1]);
    pthread_create(&hilos_productor[1], NULL, productor, (void *)&productor_ids[1]);
    sleep(1);
    printf("\t************************************************\n");    
    printf("\tcreando hilo consumidor...\n");
    pthread_create(&hilo_consumidor, NULL, consumidor, NULL);
    printf("\t************************************************\n");

    // Esperar a la señal SIGTERM
    pause();

    // Indicar al consumidor que no hay más producción
    pthread_mutex_lock(&mutex);
    fin_de_lectura = 1;
    pthread_cond_broadcast(&cond); // Cambiado a broadcast para despertar a todos los hilos en espera
    pthread_mutex_unlock(&mutex);

    for (int i = 0; i < NUM_THREAD; i++) {
        pthread_join(hilos_productor[i], NULL);
    }
     pthread_join(hilo_consumidor, NULL);   

    // Destruir los recursos utilizados
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond);

    return 0;
}

void *productor(void *arg) { 
    char *id = (char *)arg; 
    FILE *file = fopen("pg2000.txt", "r"); 
    char linea[KByte-10];            //a prueba y error me dio que tiene que ser 6 para que quepa en el buffer sino snprintf da error

    while (fgets(linea, sizeof(linea), file)) { 
        pthread_mutex_lock(&mutex); 

        if (cambio == *id) { 
            while ((a + 1) % MAX_BUFFER == conteo) { 
                pthread_cond_wait(&cond, &mutex);
            }

            snprintf(buffer[a], KByte, "P%c: --%s", *id, linea);

            a = (a + 1) % MAX_BUFFER; 

            pthread_cond_signal(&cond); 
        }

        pthread_mutex_unlock(&mutex);

        sleep(1); // Duerme durante un segundo. Esto asegura que solo se produce una linea por segundo
    }

    fclose(file); 

    return NULL;
}

void* consumidor(void* arg) {

      FILE *file = fopen("CONSUMIDOR.txt", "w"); 

    while (1) { 
        pthread_mutex_lock(&mutex); 
        pthread_cond_wait(&cond, &mutex);

        while (a == conteo && fin_de_lectura < 2 && !salir_del_bucle) {
            pthread_cond_wait(&cond, &mutex);
        }

        if ((a == conteo && fin_de_lectura >= 2) || salir_del_bucle) {
            pthread_mutex_unlock(&mutex);
            fclose(file);
            pthread_exit(NULL); // Salir de este hilo
        }
           printf("\tConsumidor:%s", buffer[conteo]);
           fputs(buffer[conteo], file); 


        conteo = (conteo + 1) % MAX_BUFFER; 
        pthread_cond_signal(&cond); 

        pthread_mutex_unlock(&mutex); 
    }

    fclose(file); 
    return NULL;
}

void sigusr_handler(int signo) {
     pthread_mutex_lock(&mutex);
    if (signo == SIGUSR1) {
        if (cambio == '1') {
            cambio = '2';
        } else {
            cambio = '1';
        }
    } else if (signo == SIGTERM) {
        printf("\t************************************************\n");
        printf("\tRecibida la señal SIGTERM.\n\tCerrando el programa...\n");
        pthread_mutex_unlock(&mutex);
        exit(EXIT_SUCCESS);
    }
    pthread_mutex_unlock(&mutex);
}
